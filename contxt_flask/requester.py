import json
import os
from typing import Union

import requests
import urllib3
from contxt.auth.machine import MachineTokenProvider

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)


class Requester:
    def __init__(
            self,
            client_id: str,
            edge_node_client_id: str,
            edge_node_client_secret: str,
            *args,
            **kwargs
    ):
        self.client_id = client_id
        self.edge_node_id = edge_node_client_id
        self.edge_node_secret = edge_node_client_secret

        self.token_provider = MachineTokenProvider(
            self.edge_node_id,
            self.edge_node_secret,
            self.client_id,
        )

    @classmethod
    def from_json(cls, json_path: Union[str, bytes, os.PathLike]):
        with open(json_path) as _:
            requester_cred = json.load(_)
        return cls(**requester_cred)

    @classmethod
    def from_env(cls, prefix=''):
        assert os.environ.get(f"{prefix}CLIENT_ID")
        assert os.environ.get(f"{prefix}EDGE_NODE_CLIENT_ID")
        assert os.environ.get(f"{prefix}EDGE_NODE_CLIENT_SECRET")
        return cls(
            client_id=os.environ.get(f"{prefix}CLIENT_ID"),
            edge_node_client_id=os.environ.get(f"{prefix}EDGE_NODE_CLIENT_ID"),
            edge_node_client_secret=os.environ.get(f"{prefix}EDGE_NODE_CLIENT_SECRET"),
        )

    def post(self, url, *args, **kwargs):
        access_token = self.token_provider.access_token
        kwargs.setdefault('headers', {})["Authorization"] = f"Bearer {access_token}"
        kwargs['verify'] = False
        response = requests.post(
            url,
            *args,
            **kwargs
        )
        return response

    def get(self, url, *args, **kwargs):
        access_token = self.token_provider.access_token
        kwargs.setdefault('headers', {})["Authorization"] = f"Bearer {access_token}"
        kwargs['verify'] = False
        response = requests.get(
            url,
            *args,
            **kwargs
        )
        return response
