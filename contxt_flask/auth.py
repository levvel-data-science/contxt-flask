import logging
from functools import wraps
from flask_restplus import Resource
from flask import request, current_app, jsonify
from contxt.auth.jwt import ContxtTokenValidator, InvalidTokenError
from jwt.exceptions import ExpiredSignatureError
from .exceptions import AuthError


logger = logging.getLogger(__name__)


def get_token_auth_header():
    """Obtains the Access Token from the Authorization Header
    """
    auth = request.headers.get("Authorization", None)
    if not auth:
        raise AuthError("AUTHORIZATION_HEADER_MISSING: Authorization header is expected", 401)

    parts = auth.split()

    if parts[0].lower() != "bearer":
        raise AuthError("INVALID_HEADER: Authorization header must start with 'Bearer'", 401)
    elif len(parts) == 1:
        raise AuthError("INVALID_HEADER: Token not found", 401)
    elif len(parts) > 2:
        raise AuthError(
            "INVALID_HEADER: Authorization header must only consist of the word 'Bearer' and the token.",
            401
        )

    token = parts[1]

    return token


class RequestValidator:
    def __init__(self, audience: str, organization: str):
        self.token_validator = ContxtTokenValidator(audience=audience)
        self.organization = organization

    def validate_request(self):
        token = get_token_auth_header()
        try:
            payload = self.token_validator.validate(token)
        except InvalidTokenError:
            raise AuthError("INVALID_TOKEN", 401)
        except ExpiredSignatureError:
            raise AuthError("INVALID_TOKEN: Expired token", 401)
        if not payload:
            raise AuthError("INVALID_HEADER: Unable to find appropriate key", 401)
        orgs = payload.get("organizations")
        if not orgs:
            raise AuthError("INVALID_PAYLOAD: Unable to find organizations in token", 401)

        if self.organization not in orgs:
            raise AuthError(
                "MISSING_ORGANIZATION: Client does not have access to the expected organization",
                401
            )

        return payload


class SecureResource(Resource):
    # registering auth decorator for all subclasses of SecureResources
    def __init__(self, api=None, *args, **kwargs):
        super(SecureResource, self).__init__(api)
        self.method_decorators = [self.requires_token_auth_generator()]
        self.request_validator = RequestValidator(
            audience=self.api.app.config["CLIENT_ID"], organization=self.api.app.config["ORGANIZATION_ID"]
        )

    def requires_token_auth_generator(self):
        def requires_token_auth(meth):
            """Token based authentication decorator"""
            @wraps(meth)
            def wrapper(*args, **kwargs):
                try:
                    logger.info('Checking auth...')
                    self.request_validator.validate_request()
                    logger.info('Authorized')
                    return meth(*args, **kwargs)
                except AuthError as ex:
                    auth_header = request.headers.get("Authorization", None)
                    logger.error("AUTHORIZATION HEADER: {auth_header}".format(auth_header=auth_header))
                    self.api.abort(401, message=ex)

            return wrapper
        return requires_token_auth


def auth_endpoint(func):
    """
    Decorator for a flask endpoint to authenticate before serving the request.
    """
    def wrapper(*args, **kwargs):
        print('checking auth')
        try:
            request_validator = RequestValidator(
                audience=current_app.config["CLIENT_ID"], organization=current_app.config["ORGANIZATION_ID"]
            )
            request_validator.validate_request()
            return func(*args, **kwargs)
        except AuthError as ex:
            auth_header = request.headers.get("Authorization", None)
            logger.error("AUTHORIZATION HEADER: {auth_header}".format(auth_header=auth_header))
            response = jsonify(ex.to_dict())
            response.status_code = ex.status_code
            return response
    return wrapper
