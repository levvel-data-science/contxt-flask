# Contxt Flask Authorization

This package attempts to create an authorization framework to easily implement Contxt authorization into any flask or 
flask-restplus app.

You can install this package by pip installing this repository. 

```shell
pip install git+https://bitbucket.org/levvel-data-science/contxt-flask
```

## Usage

Before you can use this module, you need to configure your flask app to include the `"CLIENT_ID"` and 
`"ORGANIZATION_ID"` fields for use in authorization.

Contxt will set the `"CLIENT_ID"` environment variable in the container for you and can be manually obtained from the 
Contxt frontend.

The `"ORGANIZATION_ID"` must be obtained elsewhere and manually set as an environment where the container will run.  
<!--- TODO: find out where to obtain ORGANIZATION_ID -->

To make a request to a secure endpoint you must obtain a token from the Contxt frontend and include it in the 
`Authorization` header:

```
Authorization: "Bearer <token>"
```
_Note, you must include the `Bearer ` part of the header and seperate the token from `Bearer` with a space._

### Environment Variables

Here's an example of how you might load the environment variables into your flask config:

```python
import os
from flask import Flask 

app = Flask(__name__)

app.config.update({
    "CLIENT_ID": os.environ["CLIENT_ID"],
    "ORGANIZATION_ID": os.environ["ORGANIZATION_ID"]
})
```

### Flask

Using the Flask API framework you can require authentication for an endpoint with the decorator `@auth_endpoint`.

To use the decorator, put it after the `@app.route()` decorator.
```python
from flask import Flask
from contxt_flask import auth_endpoint
app = Flask(__file__)

@app.route('/')
@auth_endpoint
def endpoint():
    ...
```

### Flask-RESTPlus

With the Flask-RESTPlus API framework, securing an endpoint is easy.

Instead of declaring an endpoint by using the `Resource` object from Flask-RESTPlus:
```python
from flask import Flask
from flask_restplus import Resource, Api
app = Flask(__file__)
api = Api(app)

@api.route('/items')
class Items(Resource):
    def post(self):
        ...
``` 
You use the `SecureResource` from this module:
```python
from flask import Flask
from flask_restplus import Api
from contxt_flask import SecureResource
app = Flask(__file__)
api = Api(app)

@api.route('/items')
class Items(SecureResource):
    def post(self):
        ...
```

Or just overwrite the `Resource` object and keep the rest of the code the same:
```python
from flask import Flask
from flask_restplus import Api
from contxt_flask import SecureResource
app = Flask(__file__)
api = Api(app)
Resource = SecureResource

@api.route('/items')
class Items(Resource):
    def post(self):
        ...
```
This will ensure that every request is authenticated.
