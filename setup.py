import setuptools

with open("README.md", "r") as file:
    long_description = file.read()

with open('requirements.txt', 'rb') as file:
    requirements = [line.strip() for line in file.read().decode("utf-8").split("\n")]

setuptools.setup(
    name="Contxt Flask Authorization",
    version="0.0.1",
    author="Levvel-DS",
    author_email="Jacob.Thompson@Levvel.io",
    description="Auth flow for Contxt API's",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://bitbucket.org/levvel-data-science/contxt-flask",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
    ],
    install_requires=requirements,
    python_requires='>=3',
    data_files=[
        ('.', ['requirements.txt'])
    ]
)